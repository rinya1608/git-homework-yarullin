from os import remove
from time import time

from flask import Flask, request, send_file

from muse_handler import harmonize, bach_style

app = Flask(__name__)


@app.route("/api/file", methods=['POST'])
def index():
    file = request.files.get('file')
    tmp_filename = f'tmp_{file.filename}_{time()}.mid'
    res_filename = f'res{file.filename}.mid'
    file.save(tmp_filename)

    generator_style = request.values.get("generatorStyle")

    if generator_style == 'chords':
        harm = harmonize(tmp_filename)
        harm.write('midi', res_filename)
    elif generator_style == 'bach':
        res_filename = bach_style(tmp_filename)

    remove(tmp_filename)
    return send_file(res_filename)


if __name__ == "__main__":
    app.run()
