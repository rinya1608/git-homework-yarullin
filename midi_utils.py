import numpy as np
import pypianoroll
import tensorflow as tf


def save_pianoroll_as_midi(pianoroll,
                           programs=[0, 0, 0, 0],
                           is_drums=[False, False, False, False],
                           tempo=100,  # in bpm
                           beat_resolution=4,  # number of time steps
                           destination_path=None
                           ):
    pianoroll = pianoroll > 0

    # Reshape batched pianoroll array to a single pianoroll array
    p_roll = pianoroll.reshape((-1, pianoroll.shape[2], pianoroll.shape[3]))

    # Create the tracks
    tracks = []
    for idx in range(p_roll.shape[2]):
        tracks.append(pypianoroll.Track(
            p_roll[..., idx], programs[idx], is_drums[idx]))

    multitrack = pypianoroll.Multitrack(
        tracks=tracks, tempo=tempo, beat_resolution=beat_resolution)

    multitrack.write(destination_path)
    print('Midi saved to ', destination_path)
    return destination_path


def get_conditioned_track(midi=None, phrase_length=32, beat_resolution=4):
    if not isinstance(midi, str):
        # ----------- Generation from preprocessed dataset ------------------
        sample_x = midi
        sample_c = np.expand_dims(sample_x[..., 0], -1)
    else:
        # --------------- Generation from midi file -------------------------
        midi_file = midi
        parsed = pypianoroll.Multitrack(beat_resolution=beat_resolution)
        parsed.parse_midi(midi_file)

        sample_c = parsed.tracks[0].pianoroll.astype(np.float32)

        samples = []
        for i in range(len(sample_c) // phrase_length):
            sample_part = sample_c[i * phrase_length:(i + 1) * phrase_length]
            sample_part[sample_part > 0] = 1
            sample_part[sample_part <= 0] = -1
            sample_part = np.expand_dims(np.expand_dims(sample_part, 0), -1)
            sample_part = tf.convert_to_tensor(sample_part, dtype=tf.float32)
            samples.append(sample_part)

    return samples
